package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Profile("MyDao")
@Repository
public class StudentDaoImplSecond implements StudentDao {

    List<Student> students;

    public StudentDaoImplSecond() {
        this.students = new ArrayList<>();
        this.students.add(Student.builder()
            .id(4l)
            .studentId("SE-004")
            .name("Prophet")
            .surname("No")
            .gpa(4.00)
            .image("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTeARxF72miCJ8P6M5Hv9qxr87DSFLU4SFc-aE1-9O3JLZpV0lo")
            .penAmount(9000)
            .description("It's over 9000!!!")
            .build());
        this.students.add(Student.builder()
                .id(5l)
                .studentId("SE-005")
                .name("Cry")
                .surname("Cat")
                .gpa(0.30)
                .image("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSLUpk7AHPVNyRETs_h4G6CGybKeaByD9pmmRJisfuuZ6NB9Z5a")
                .penAmount(333)
                .description("14 Year old girl")
                .build());
        this.students.add(Student.builder()
                .id(6l)
                .studentId("SE-006")
                .name("Pika")
                .surname("Chu?")
                .gpa(1.50)
                .image("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQwod_DahUn5dSKg8DOu0posanpzrN3hk0LwtVVzdAwlOEOMH6Q")
                .penAmount(555)
                .description("BE LIKE")
                .build());

    }

    @Override
    public List<Student> getAllStudent() {
        log.info("My dao is called");
        return students;
    }

    @Override
    public Student findById(Long id) {
        return students.get((int) (id -1));
    }

    @Override
    public Student saveStudent(Student student) {
        student.setId((long) students.size());
        students.add(student);
        return student;
    }
}
